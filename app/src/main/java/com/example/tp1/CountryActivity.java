package com.example.tp1;


import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class CountryActivity extends AppCompatActivity {
    CountryList cl;
    ImageView image;
    TextView countryname;
    EditText capitale;
    EditText langue;
    EditText monnaie;
    EditText population;
    EditText superficie;
    Button sauvgarder;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_country);

        cl= new CountryList();

        image=(ImageView) findViewById(R.id.imageView);
        countryname=(TextView) findViewById(R.id.textView);
        langue=(EditText) findViewById(R.id.langue);
        capitale=(EditText) findViewById(R.id.capitale);
        monnaie=(EditText) findViewById(R.id.monnaie);
        population=(EditText) findViewById(R.id.population);
        superficie=(EditText) findViewById(R.id.superficie);
        sauvgarder=(Button) findViewById(R.id.button);
        Bundle extras = getIntent().getExtras();


        final Country country = cl.getCountry(extras.getString("country"));


        int id = getResources().getIdentifier("com.example.tp1:drawable/" +country.getmImgFile() , null, null);
        image.setImageResource(id);
        countryname.setText(extras.getString("country"));
        capitale.setText(country.getmCapital());
        langue.setText(country.getmLanguage());
        monnaie.setText(country.getmCurrency());

        population.setText(String.valueOf(country.getmPopulation()));
        superficie.setText(String.valueOf(country.getmArea()));

        sauvgarder.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // TODO Auto-generated method stub
                country.setmCapital(capitale.getText().toString());
                country.setmLanguage(langue.getText().toString());
                //Toast.makeText("jfhf",,,);
            }
        });

    }
}
