package com.example.tp1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toolbar;


public class MainActivity extends AppCompatActivity {

    ListView listview;
    Toolbar toolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        listview=(ListView)findViewById(R.id.listview);



        String[] countrys = CountryList.getNameArray();
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,android.R.layout.simple_spinner_item,countrys);
        listview.setAdapter(adapter);


        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                String country =parent.getAdapter().getItem(position).toString();

                Intent intent = new Intent(MainActivity.this, CountryActivity.class);
                intent.putExtra("country",country);
                startActivity(intent);

            }

        });

    }
}

